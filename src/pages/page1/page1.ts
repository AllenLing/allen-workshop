import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {

  userData: any;
  abc: boolean = true;
  users: Array<any> = [{name: "Allen", tel: "5359"},
  					  {name: "Allen2", tel: "1234"}]	
  constructor(public navCtrl: NavController, private params: NavParams) {
    this.userData = params.get("userData");
    console.log(this.userData);
  }

  change(){
  	this.users[1].name = "Eric";
  	this.abc = false;
  }

}
