import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Page1 } from '../page1/page1'
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { TabsPage } from '../tabs/tabs';



/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  
  loginData = { account:'T2988', password: ''};

  constructor(public http: Http, public navCtrl: NavController) {}


  login(){
    var body ={
      username: this.loginData.account,
      password: this.loginData.password
    }


    var contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');

    this.http.post('https://service.testritegroup.com/api/adAuth', body, { headers: contentHeaders })
      .subscribe(
        response => {
          console.log(response);
          let message=response.json().message;
          console.log(response.json().data);

            if ("success"===message){
             this.navCtrl.push(Page1, {"userData" : response.json().data});
           }else{
                console.log(" step error");
              }
        },
        error => {
         alert(error.text());
          console.log(error.text());
        }
      );
  }

}
