import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Page1 } from '../page1/page1';
import { Page2 } from '../page2/page2';
import { LoginPage } from '../login/login';

/*
  Generated class for the Tabs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: any;
  tab2Root: any = Page2;
  tab3Root: any = LoginPage;
  user: number = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	if(this.user === 1){
  		this.tab1Root = Page1;
  	}else{
  		this.tab1Root = Page2;
  	}
  }



}
